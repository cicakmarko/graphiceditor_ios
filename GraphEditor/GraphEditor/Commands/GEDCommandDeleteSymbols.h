//
// Created by cicakmarko on 10/29/13.
//
//  Copyright (c) 2013 CenterDevice GmbH. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GEDCommandAbstract.h"


@interface GEDCommandDeleteSymbols : GEDCommandAbstract

- (instancetype) initWithSymbols:(NSArray*)symbols;

@end
