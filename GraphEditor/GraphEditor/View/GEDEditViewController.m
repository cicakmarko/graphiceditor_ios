//
// Created by cicakmarko on 6/16/13.
//
//  Copyright (c) 2013 CenterDevice GmbH. All rights reserved.
//

#import "GEDEditViewController.h"
#import "GEDActionManager.h"
#import "GEDActionAbstract.h"
#import "GEDActionEditSelectAll.h"
#import "GEDActionEditCut.h"
#import "GEDActionEditCopy.h"
#import "GEDActionEditPaste.h"
#import "GEDActionEditRedo.h"
#import "GEDActionEditUndo.h"

typedef NS_ENUM(NSUInteger, GEDEditOption)
{
    GEDEditOptionUndo,
    GEDEditOptionRedo,
    GEDEditOptionCut,
    GEDEditOptionCopy,
    GEDEditOptionPaste,
    GEDEditOptionSelectAll,
    GEDEditOptionPreferences
};

@interface GEDEditViewController () <UITableViewDelegate>
@end

@implementation GEDEditViewController

#pragma mark - UITableViewDelegate

- (void) tableView:(UITableView*)tableView didSelectRowAtIndexPath:(NSIndexPath*)indexPath
{
    switch (indexPath.item)
    {
        case GEDEditOptionUndo:
        {
            [[SERRY.actionManager undoAction] perform];
        }
            break;
        case GEDEditOptionRedo:
        {
            [[SERRY.actionManager redoAction] perform];
        }
            break;
        case GEDEditOptionCut:
        {
            [[SERRY.actionManager cutAction] perform];
        }
            break;
        case GEDEditOptionCopy:
        {
            [[SERRY.actionManager copyAction] perform];
        }
            break;
        case GEDEditOptionPaste:
        {
            [[SERRY.actionManager pasteAction] perform];
        }
            break;
        case GEDEditOptionSelectAll:
        {
            [[SERRY.actionManager selectAllAction] perform];
        }
            break;
        case GEDEditOptionPreferences:
        {
            // nothing yet
        }
            break;
        default:
            [NSException raise:IllegalArgumentException format:@"Unsupported option: %d", indexPath.item];
    }

    [SERRY.context.popoverController dismissPopoverAnimated:YES];
}

@end
