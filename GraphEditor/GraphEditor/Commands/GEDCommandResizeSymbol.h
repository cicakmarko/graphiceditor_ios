//
// Created by cicakmarko on 10/29/13.
//
//  Copyright (c) 2013 CenterDevice GmbH. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GEDCommandAbstract.h"


@interface GEDCommandResizeSymbol : GEDCommandAbstract

- (instancetype) initWithSymbol:(GEDSymbol*)symbol
                startDimensions:(CGRect)startDimensions
                  endDimensions:(CGRect)endDimensions;

@end
