//
// Created by cicakmarko on 10/23/13.
//
//  Copyright (c) 2013 CenterDevice GmbH. All rights reserved.
//

#import "GEDActionResetZoom.h"
#import "GEDView.h"


@implementation GEDActionResetZoom

- (void) performIfNoPopoverVisible
{
    SERRY.context.scale = 1.0f;
    SERRY.context.transform = CGAffineTransformIdentity;
    [SERRY.model fireModelUpdate];
}

@end
