//
// Created by cicakmarko on 10/28/13.
//
//  Copyright (c) 2013 CenterDevice GmbH. All rights reserved.
//

#import "GEDCommandManager.h"
#import "GEDCommand.h"

@interface GEDCommandManager ()
@property(nonatomic, strong) NSMutableArray* commands;
@property(nonatomic, assign) NSUInteger currentCommandIndex;
@end


@implementation GEDCommandManager

- (id) init
{
    self = [super init];
    if (self)
    {
        _commands = [NSMutableArray array];
        _currentCommandIndex = 0;
    }

    return self;
}

- (void) addCommand:(id <GEDCommand>)command
{
    while (self.currentCommandIndex < self.commands.count)
    {
        [self.commands removeObjectAtIndex:self.currentCommandIndex];
    }
    [self.commands addObject:command];
    [self doCommand];
}

- (void) doCommand
{
    if (self.currentCommandIndex < self.commands.count)
    {
        [[self.commands objectAtIndex:self.currentCommandIndex++] doCommand];
    }
}

- (void) undoCommand
{
    if (self.currentCommandIndex > 0)
    {
        [[self.commands objectAtIndex:--self.currentCommandIndex] undoCommand];
    }
}

- (BOOL) currentCommandIsFirst
{
    return self.currentCommandIndex == 0;
}

- (BOOL) currentCommandIsLast
{
    return self.currentCommandIndex == self.commands.count;
}

- (BOOL) isEmpty
{
    return self.commands.count == 0;
}

- (NSUInteger) count
{
    return self.commands.count;
}

@end
