//
// Created by cicakmarko on 10/25/13.
//
//  Copyright (c) 2013 CenterDevice GmbH. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface GEDSelectionModel : NSObject

- (void) addToSelection:(GEDSymbol*)symbol;

- (void) addMultipleSelection:(NSArray*)symbols;

- (void) removeFromSelection:(GEDSymbol*)symbol;

- (void) clearSelection;

- (NSArray*) elements;

- (NSUInteger) count;

- (BOOL) isEmpty;

@end
