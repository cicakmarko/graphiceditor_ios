//
// Created by cicakmarko on 6/14/13.
//
//  Copyright (c) 2013 CenterDevice GmbH. All rights reserved.
//

#import <Foundation/Foundation.h>

@class GEDActionDeleteAll;
@class GEDActionDeleteSelection;
@class GEDActionMenuEdit;
@class GEDActionEditSelectAll;
@class GEDActionResetZoom;
@class GEDActionMenuFit;
@class GEDActionZoomToFit;
@class GEDActionEditCut;
@class GEDActionEditCopy;
@class GEDActionEditPaste;
@class GEDActionEditUndo;
@class GEDActionEditRedo;

@interface GEDActionManager : NSObject

- (GEDActionDeleteAll*) deleteAllAction;

- (GEDActionDeleteSelection*) deleteSelectionAction;

- (GEDActionMenuEdit*) menuEditActionWithAnchor:(id)anchor;

- (GEDActionEditSelectAll*) selectAllAction;

- (GEDActionResetZoom*) resetZoomAction;

- (GEDActionMenuFit*) fitActionWithAnchor:(UIBarButtonItem*)anchor;

- (GEDActionZoomToFit*) fitZoomActionWithElements:(NSArray*)elements;

- (GEDActionEditCut*) cutAction;

- (GEDActionEditCopy*) copyAction;

- (GEDActionEditPaste*) pasteAction;

- (GEDActionEditUndo*) undoAction;

- (GEDActionEditRedo*) redoAction;

@end
