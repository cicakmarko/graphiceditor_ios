//
// Created by cicakmarko on 10/26/13.
//
//  Copyright (c) 2013 CenterDevice GmbH. All rights reserved.
//

@interface GEDSymbolPainter : NSObject

@property(nonatomic, strong) GEDSymbol* symbol;

- (instancetype) initWithSymbol:(GEDSymbol*)symbol;

- (void) draw:(CGContextRef)context;

@end
