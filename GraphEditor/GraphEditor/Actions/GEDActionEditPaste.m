//
// Created by cicakmarko on 10/25/13.
//
//  Copyright (c) 2013 CenterDevice GmbH. All rights reserved.
//

#import "GEDActionAbstract.h"
#import "GEDActionEditPaste.h"
#import "GEDClipboard.h"
#import "GEDSelectionModel.h"
#import "GEDCommandAddSymbols.h"
#import "GEDCommandManager.h"

@implementation GEDActionEditPaste

- (void) perform
{
    NSArray* pastedElements = [SERRY.clipboard content];

    // shift all symbols by 50 points
    for (GEDSymbol* symbol in pastedElements)
    {
        CGPoint originalPosition = symbol.position;
        symbol.position = CGPointMake(originalPosition.x + 50, originalPosition.y + 50);
    }

    GEDCommandAddSymbols* pasteCommand = [[GEDCommandAddSymbols alloc] initWithSymbols:pastedElements];
    [SERRY.commandManager addCommand:pasteCommand];
    [SERRY.selection addMultipleSelection:pastedElements];
}

@end
