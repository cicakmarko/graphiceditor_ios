//
//  GEDModelHelper.h
//  GraphEditor
//
//  Created by Marko Cicak on 10/31/12.
//  Copyright (c) 2012 Marko Cicak. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GEDModel.h"
#import "GEDSymbol.h"

@interface GEDModelHelper : NSObject

/*
 Method iterates (backwards) through GEDView's symbol painters in order to find the top most element.
 @return top-most visible element at given point. If there is no element under point, nil is returned.
 */
+ (GEDSymbol*) symbolAtPoint:(CGPoint)point;

// TODO: implement isVisible

@end
