//
// Created by Marko Cicak on 11/1/15.
// Copyright (c) 2015 Marko Cicak. All rights reserved.
//

#import "GEDState.h"

@implementation GEDState

- (void) stateStarted
{

}

- (void) stateFinished
{

}

- (void) pinchEnded:(UIPinchGestureRecognizer*)recognizer
{

}

- (void) pinchChanged:(UIPinchGestureRecognizer*)recognizer
{

}

- (void) pinchBegan:(UIPinchGestureRecognizer*)recognizer
{

}

- (void) tapBegan:(UITapGestureRecognizer*)recognizer
{

}

- (void) tapChanged:(UITapGestureRecognizer*)recognizer
{

}

- (void) tapEnded:(UITapGestureRecognizer*)recognizer
{

}

- (void) panBegan:(UIPanGestureRecognizer*)recognizer
{

}

- (void) panChanged:(UIPanGestureRecognizer*)recognizer
{

}

- (void) panEnded:(UIPanGestureRecognizer*)recognizer
{

}

- (CGPoint) transformToUserSpace:(CGPoint)point
{
    return CGPointApplyAffineTransform(point, CGAffineTransformInvert(SERRY.context.transform));
}

@end
