//
// Created by cicakmarko on 10/23/13.
//
//  Copyright (c) 2013 CenterDevice GmbH. All rights reserved.
//

#import "GEDActionManager.h"
#import "GEDClipboard.h"
#import "GEDSelectionModel.h"
#import "GEDCommandManager.h"

@implementation GEDServiceRegistry

static GEDServiceRegistry* _instance = nil;

- (instancetype) init
{
    if (self = [super init])
    {
        _model = [[GEDModel alloc] init];
        _context = [[GEDContext alloc] init];
        _clipboard = [[GEDClipboard alloc] init];
        _selection = [[GEDSelectionModel alloc] init];
        _actionManager = [[GEDActionManager alloc] init];
        _commandManager = [[GEDCommandManager alloc] init];
    }
    return self;
}

+ (GEDServiceRegistry*) instance
{
    if (!_instance)
    {
        _instance = [[self alloc] init];
    }

    return _instance;
}

@end
