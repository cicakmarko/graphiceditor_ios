//
// Created by cicakmarko on 6/14/13.
//
//  Copyright (c) 2013 CenterDevice GmbH. All rights reserved.
//

#import "GEDActionDeleteSelection.h"
#import "GEDSelectionModel.h"
#import "GEDCommandDeleteSymbols.h"
#import "GEDCommandManager.h"

@implementation GEDActionDeleteSelection

- (void) perform
{
    GEDCommandDeleteSymbols* deleteCommand = [[GEDCommandDeleteSymbols alloc] initWithSymbols:SERRY.selection.elements];
    [SERRY.commandManager addCommand:deleteCommand];
}

@end
