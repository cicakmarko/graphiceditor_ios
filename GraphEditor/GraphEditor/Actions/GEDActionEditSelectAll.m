//
// Created by cicakmarko on 10/23/13.
//
//  Copyright (c) 2013 CenterDevice GmbH. All rights reserved.
//

#import "GEDActionEditSelectAll.h"
#import "GEDSelectionModel.h"


@implementation GEDActionEditSelectAll

- (void) perform
{
    [SERRY.selection addMultipleSelection:SERRY.model.symbols];
}

@end
