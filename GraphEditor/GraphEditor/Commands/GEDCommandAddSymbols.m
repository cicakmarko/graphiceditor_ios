//
// Created by cicakmarko on 10/28/13.
//
//  Copyright (c) 2013 CenterDevice GmbH. All rights reserved.
//

#import "GEDCommandAddSymbols.h"
#import "GEDSelectionModel.h"

@interface GEDCommandAddSymbols ()
@property(nonatomic, strong) NSArray* symbols;
@end


@implementation GEDCommandAddSymbols

- (instancetype) initWithSymbols:(NSArray*)symbols
{
    if (self = [super init])
    {
        _symbols = [symbols copy];
    }
    return self;
}

- (void) doCommand
{
    [SERRY.selection clearSelection];
    [SERRY.model addSymbols:self.symbols];
}

- (void) undoCommand
{
    [SERRY.selection clearSelection];
    [SERRY.model removeSymbols:self.symbols];
}

@end
