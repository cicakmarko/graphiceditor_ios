//
//  GEDSelectionHandleHandler.h
//  GraphEditor
//
//  Created by Marko Cicak on 11/1/12.
//  Copyright (c) 2012 Marko Cicak. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GEDSymbol.h"

#define HANDLE_RENDER_SIZE 16
#define HANDLE_TOUCH_SIZE 44

typedef enum
{
    SymbolHandleNone, // no handle selected
    SymbolHandleNorth,
    SymbolHandleSouth,
    SymbolHandleEast,
    SymbolHandleWest,
    SymbolHandleSouthEast,
    SymbolHandleSouthWest,
    SymbolHandleNorthEast,
    SymbolHandleNorthWest
} SymbolHandle; // handles for resizing symbols

@interface GEDSelectionHandleHandler : NSObject

/**
 * Drawing selection handles around selected symbols. Handle size cannot depend  on scale.
 */
+ (void) paintSelectionHandles:(CGContextRef)context
                 withSelection:(GEDSelectionModel*)selection
                      andScale:(CGFloat)scale;

/*
 * Returns a handle for given point if one is hit, otherwise returns nil.
 */
+ (SymbolHandle) getHandleForSymbol:(GEDSymbol*)symbol point:(CGPoint)point scale:(CGFloat)scale;

@end
