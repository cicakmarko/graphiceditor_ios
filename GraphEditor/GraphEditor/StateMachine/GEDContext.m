//
// Created by Marko Cicak on 11/1/15.
// Copyright (c) 2015 Marko Cicak. All rights reserved.
//

#import "GEDContext.h"
#import "GEDSymbol.h"
#import "GEDView.h"


@implementation GEDContext

- (instancetype) init
{
    if (self = [super init])
    {
        _lasoOn = NO;
        _scale = 1.0f;
        _transform = CGAffineTransformIdentity;
        _symbolIndex = 1;
    }
    return self;
}

@end
