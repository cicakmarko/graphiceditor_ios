//
//  GEDView.m
//  GraphEditor
//
//  Created by Marko Cicak on 10/30/12.
//  Copyright (c) 2012 Marko Cicak. All rights reserved.
//

#import "GEDView.h"
#import "GEDSelectionHandleHandler.h"
#import "GEDSymbolPainter.h"
#import "GEDPainterRectangle.h"
#import "GEDSelectionState.h"

@interface GEDView ()
@property(nonatomic, strong) NSMutableArray* elementPainters;
@end


@implementation GEDView

- (instancetype) initWithCoder:(NSCoder*)aDecoder
{
    if (self = [super initWithCoder:aDecoder])
    {
        [self setupInstance];
    }
    return self;
}

- (void) setupInstance
{
    _elementPainters = [NSMutableArray array];
    self.currentState = [[GEDSelectionState alloc] init];
    SERRY.context.graphView = self;
    GEDNC_ADD_OBSERVER_SELF(@selector(didAddSymbols:), GEDSymbolsAddedNotification, nil);
    GEDNC_ADD_OBSERVER_SELF(@selector(didRemoveSymbols:), GEDSymbolsRemovedNotification, nil);
    GEDNC_ADD_OBSERVER_SELF(@selector(didSelectSymbols:), GEDSelectionChangedNotification, nil);
    GEDNC_ADD_OBSERVER_SELF(@selector(didUpdateModel:), GEDModelUpdatedNotification, nil);

    [self addGestureRecognizer:[UIPinchGestureRecognizer.alloc initWithTarget:self action:@selector(didPinch:)]];
    [self addGestureRecognizer:[UITapGestureRecognizer.alloc initWithTarget:self action:@selector(didTap:)]];
    [self addGestureRecognizer:[UIPanGestureRecognizer.alloc initWithTarget:self action:@selector(didPan:)]];
}

// in order to keep state machine clean of UIRecognizers, artificial gesture is created and sent as iOS native gesture
- (void) touchesBegan:(NSSet<UITouch*>*)touches withEvent:(UIEvent*)event
{
    GEDTapBeganGesture* gesture = GEDTapBeganGesture.new;
    gesture.location = [[touches anyObject] locationInView:self];
    [self.currentState tapBegan:gesture];
}

- (void) didPinch:(UIPinchGestureRecognizer*)gesture
{
    switch (gesture.state)
    {
        case UIGestureRecognizerStateBegan:
            [self.currentState pinchBegan:gesture];
            break;
        case UIGestureRecognizerStateChanged:
            [self.currentState pinchChanged:gesture];
            break;
        case UIGestureRecognizerStateEnded:
        case UIGestureRecognizerStateCancelled:
        case UIGestureRecognizerStateFailed:
            [self.currentState pinchEnded:gesture];
            break;
        case UIGestureRecognizerStatePossible:
            break;
    }
}

- (void) didTap:(UITapGestureRecognizer*)gesture
{
    NSLog(@"TAP: %d", gesture.state);
    switch (gesture.state)
    {
        case UIGestureRecognizerStateBegan:
            [self.currentState tapBegan:gesture];
            break;
        case UIGestureRecognizerStateChanged:
            [self.currentState tapChanged:gesture];
            break;
        case UIGestureRecognizerStateEnded:
        case UIGestureRecognizerStateCancelled:
        case UIGestureRecognizerStateFailed:
            [self.currentState tapEnded:gesture];
            break;
        case UIGestureRecognizerStatePossible:
            break;
    }
}

- (void) didPan:(UIPanGestureRecognizer*)gesture
{
    NSLog(@"PAN: %d", gesture.state);
    switch (gesture.state)
    {
        case UIGestureRecognizerStateBegan:
            [self.currentState panBegan:gesture];
            break;
        case UIGestureRecognizerStateChanged:
            [self.currentState panChanged:gesture];
            break;
        case UIGestureRecognizerStateEnded:
        case UIGestureRecognizerStateCancelled:
        case UIGestureRecognizerStateFailed:
            [self.currentState panEnded:gesture];
            break;
        case UIGestureRecognizerStatePossible:
            break;
    }
}

- (void) setCurrentState:(GEDState*)currentState
{
    [self.currentState stateFinished];
    _currentState = currentState;
    [self.currentState stateStarted];
}

- (void) drawRect:(CGRect)rect
{
    CGContextRef currentContext = UIGraphicsGetCurrentContext();
    CGContextSaveGState(currentContext);

    for (GEDSymbolPainter* painter in self.elementPainters)
    {
        [painter draw:currentContext];
    }

    [GEDSelectionHandleHandler paintSelectionHandles:currentContext
                                       withSelection:SERRY.selection
                                            andScale:SERRY.context.scale];

    // draw laso selection
    if (SERRY.context.lasoOn)
    {
        CGFloat lengths[2];
        lengths[0] = 2;
        lengths[1] = 3;
        CGContextSetLineDash(currentContext, 0.0f, lengths, 2);
        CGContextSetLineWidth(currentContext, 2);
        CGContextSetRGBStrokeColor(currentContext, 0, 0, 0, 1);
        CGContextStrokeRect(currentContext, SERRY.context.laso);
    }

    CGContextRestoreGState(currentContext);
}

- (void) redraw
{
    [self performSelectorOnMainThread:@selector(setNeedsDisplay) withObject:nil waitUntilDone:YES];
}

- (NSArray*) painters
{
    return [NSArray arrayWithArray:self.elementPainters];
}

#pragma mark - Notifications

- (void) didAddSymbols:(NSNotification*)notification
{
    // TODO: need generic solution
    NSArray* symbols = notification.userInfo[@"symbols"];
    for (GEDSymbol* symbol in symbols)
    {
        GEDPainterRectangle* painter = [[GEDPainterRectangle alloc] initWithSymbol:symbol];
        [self.elementPainters addObject:painter];
    }

    [self redraw];
}

- (void) didRemoveSymbols:(NSNotification*)notification
{
    [self.elementPainters removeAllObjects];
    for (GEDSymbol* symbol in SERRY.model.symbols)
    {
        [self.elementPainters addObject:[[GEDPainterRectangle alloc] initWithSymbol:symbol]];
    }
    [self redraw];
}

- (void) didSelectSymbols:(NSNotification*)notification
{
    NSArray* selectedSymbols = notification.userInfo[@"symbols"];
    [self drawLast:selectedSymbols];
    [self redraw];
}

- (void) didUpdateModel:(NSNotification*)notification
{
    [self redraw];
}

#pragma mark - Private

- (void) drawLast:(NSArray*)selectedSymbols
{
    // Place selected elements last to draw to seem as if they're on top
    NSMutableArray* selectedPainters = [NSMutableArray array];

    for (GEDSymbol* symbol in selectedSymbols)
    {
        for (GEDSymbolPainter* painter in self.elementPainters)
        {
            if (symbol == painter.symbol)
            {
                [selectedPainters addObject:painter];
            }
        }
    }

    [self.elementPainters removeObjectsInArray:selectedPainters];
    [self.elementPainters addObjectsFromArray:selectedPainters];
}

@end

@implementation GEDTapBeganGesture

- (CGPoint) locationInView:(UIView*)view
{
    return self.location;
}

@end
