//
//  GEDFitViewController.m
//  GraphEditor
//
//  Created by Marko Cicak on 10/24/13.
//  Copyright (c) 2013 Marko Cicak. All rights reserved.
//

#import "GEDFitViewController.h"
#import "GEDActionManager.h"
#import "GEDActionZoomToFit.h"
#import "GEDSelectionModel.h"

typedef NS_ENUM(NSUInteger, GEDFitOption)
{
    GEDFitOptionSelection,
    GEDFitOptionAll
};


@interface GEDFitViewController () <UITableViewDelegate>
@end

@implementation GEDFitViewController

#pragma mark - UITableViewDelegate

- (void) tableView:(UITableView*)tableView didSelectRowAtIndexPath:(NSIndexPath*)indexPath
{
    switch (indexPath.item)
    {
        case GEDFitOptionSelection:
        {
            [[SERRY.actionManager fitZoomActionWithElements:SERRY.selection.elements] perform];
        }
            break;
        case GEDFitOptionAll:
        {
            [[SERRY.actionManager fitZoomActionWithElements:SERRY.model.symbols] perform];
        }
            break;
        default:
            [NSException raise:IllegalArgumentException format:@"Unsupported option: %d", indexPath.item];
    }

    [SERRY.context.popoverController dismissPopoverAnimated:YES];
}

@end
