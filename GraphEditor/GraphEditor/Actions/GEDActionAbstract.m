//
// Created by cicakmarko on 6/14/13.
//
//  Copyright (c) 2013 CenterDevice GmbH. All rights reserved.
//

#import "GEDActionAbstract.h"

@implementation GEDActionAbstract

- (void) perform
{
    if ([SERRY.context.popoverController isPopoverVisible])
    {
        [SERRY.context.popoverController dismissPopoverAnimated:YES];
    }
    else if ([SERRY.context.actionSheet isVisible])
    {
        [SERRY.context.actionSheet dismissWithClickedButtonIndex:-1 animated:YES];
    }
    else
    {
        [self performIfNoPopoverVisible];
    }
}

- (void) performHide
{
    GED_THROW_ABSTRACT_METHOD_EXCEPTION;
}

- (void) performIfNoPopoverVisible
{
    GED_THROW_ABSTRACT_METHOD_EXCEPTION;
}

- (void) cancel
{
    GEDNC_REMOVE_OBSERVER_SELF;
}

- (void) presentInPopover:(UIViewController*)controller fromAnchor:(id)anchor
{
    [self presentInPopover:controller fromAnchor:anchor popoverDelegate:nil];
}

- (void) presentInPopover:(UIViewController*)controller fromAnchor:(id)anchor popoverDelegate:(id <UIPopoverControllerDelegate>)delegate
{
    UIPopoverController* popover = [[UIPopoverController alloc] initWithContentViewController:controller];
    if (delegate)
    {
        popover.delegate = delegate;
    }
    if ([anchor isKindOfClass:[UIBarButtonItem class]])
    {
        SERRY.context.popoverController = popover;
        [popover presentPopoverFromBarButtonItem:anchor
                        permittedArrowDirections:UIPopoverArrowDirectionAny
                                        animated:YES];
    }
    else if ([anchor isKindOfClass:[UIView class]])
    {
        SERRY.context.popoverController = popover;
        [popover presentPopoverFromRect:[anchor bounds] inView:anchor
               permittedArrowDirections:UIPopoverArrowDirectionAny
                               animated:YES];
    }
    else
    {
        NSLog(@"Trying to show popover from neither UIBarButtonItem nor UIView-Rect, but from %@", anchor);
    }

}
@end
