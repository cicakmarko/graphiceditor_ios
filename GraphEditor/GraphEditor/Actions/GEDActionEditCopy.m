//
// Created by cicakmarko on 10/25/13.
//
//  Copyright (c) 2013 CenterDevice GmbH. All rights reserved.
//

#import "GEDActionAbstract.h"
#import "GEDActionEditCopy.h"
#import "GEDClipboard.h"
#import "GEDSelectionModel.h"


@implementation GEDActionEditCopy

- (void) perform
{
    [SERRY.clipboard setContent:SERRY.selection.elements];
}

@end
