//
// Created by cicakmarko on 10/25/13.
//
//  Copyright (c) 2013 CenterDevice GmbH. All rights reserved.
//

#import "GEDSelectionModel.h"

@interface GEDSelectionModel ()
@property(nonatomic, strong) NSMutableArray* selection;
@end

@implementation GEDSelectionModel

- (id) init
{
    self = [super init];
    if (self)
    {
        _selection = [NSMutableArray array];
    }

    return self;
}

- (void) addToSelection:(GEDSymbol*)symbol
{
    [self.selection addObject:symbol];
    [self fireSelectionChanged:@[ symbol ]];
}

- (void) addMultipleSelection:(NSArray*)symbols
{
    for (GEDSymbol* s in symbols)
    {
        if (![self.selection containsObject:s])
        {
            [self.selection addObject:s];
        }
    }

    [self fireSelectionChanged:symbols];
}

- (void) removeFromSelection:(GEDSymbol*)symbol
{
    if ([self.selection containsObject:symbol])
    {
        [self.selection removeObject:symbol];
        [self fireSelectionChanged:@[ ]];
    }
}

- (void) clearSelection
{
    [self.selection removeAllObjects];
    [self fireSelectionChanged:@[ ]];
}

- (NSArray*) elements
{
    return [NSArray arrayWithArray:self.selection];
}

- (NSUInteger) count
{
    return self.selection.count;
}

- (BOOL) isEmpty
{
    return self.elements.count == 0;
}

#pragma mark - Private

- (void) fireSelectionChanged:(NSArray*)symbols
{
    GEDNC_POST_NOTIFICATION(GEDSelectionChangedNotification, nil, @{ @"symbols" : symbols });
}

@end
