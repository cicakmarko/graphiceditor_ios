//
// Created by Marko Cicak on 11/1/15.
// Copyright (c) 2015 Marko Cicak. All rights reserved.
//

#import "GEDResizeState.h"
#import "GEDCommandManager.h"
#import "GEDCommandResizeSymbol.h"
#import "GEDSelectionState.h"
#import "GEDView.h"

@interface GEDResizeState ()
@property(nonatomic, assign) CGPoint oldPosition;
@property(nonatomic, assign) CGPoint newPosition;
@property(nonatomic, assign) CGRect initialDimensions;
@end

@implementation GEDResizeState

- (void) stateStarted
{
    CGPoint symbolPos = SERRY.context.symbolHit.position;
    CGPoint lastPosition = SERRY.context.lastPosition;
    self.initialDimensions = [SERRY.context.symbolHit getAsRectangle];

    switch (self.handle)
    {
        case SymbolHandleNorthWest:
            self.newPosition = symbolPos;
            break;

        case SymbolHandleWest:
            self.newPosition = CGPointMake(symbolPos.x, lastPosition.y);
            break;

        case SymbolHandleSouthWest:
            self.newPosition = CGPointMake(symbolPos.x, lastPosition.y);
            break;

        case SymbolHandleNorth:
            self.newPosition = CGPointMake(lastPosition.x, symbolPos.y);
            break;

        case SymbolHandleNorthEast:
            self.newPosition = CGPointMake(lastPosition.x, symbolPos.y);
            break;

        default:
            self.newPosition = SERRY.context.lastPosition;
            break;
    }
}

- (void) panChanged:(UIPanGestureRecognizer*)recognizer
{
    GEDSymbol* selected = SERRY.context.symbolHit;

    self.oldPosition = self.newPosition;
    self.newPosition = [self transformToUserSpace:[recognizer locationInView:(id) SERRY.context.graphView]];

    CGFloat dx = self.newPosition.x - self.oldPosition.x;
    CGFloat dy = self.newPosition.y - self.oldPosition.y;

    switch (self.handle)
    {
        case SymbolHandleNorthWest:
            selected.position = self.newPosition;
            selected.size = CGSizeMake(selected.size.width - dx, selected.size.height - dy);
            break;

        case SymbolHandleNorth:
            selected.position = CGPointMake(selected.position.x, self.newPosition.y);
            selected.size = CGSizeMake(selected.size.width, selected.size.height - dy);
            break;

        case SymbolHandleNorthEast:
            selected.position = CGPointMake(selected.position.x, self.newPosition.y);
            selected.size = CGSizeMake(selected.size.width + dx, selected.size.height - dy);
            break;

        case SymbolHandleEast:
            selected.size = CGSizeMake(selected.size.width + dx, selected.size.height);
            break;

        case SymbolHandleSouthEast:
            selected.size = CGSizeMake(selected.size.width + dx, selected.size.height + dy);
            break;

        case SymbolHandleSouth:
            selected.size = CGSizeMake(selected.size.width, selected.size.height + dy);
            break;

        case SymbolHandleSouthWest:
            selected.position = CGPointMake(self.newPosition.x, selected.position.y);
            selected.size = CGSizeMake(selected.size.width - dx, selected.size.height + dy);
            break;

        case SymbolHandleWest:
            selected.position = CGPointMake(self.newPosition.x, selected.position.y);
            selected.size = CGSizeMake(selected.size.width - dx, selected.size.height);
            break;

        default:
            return;
    }

    [SERRY.model fireModelUpdate];
}

- (void) panEnded:(UIPanGestureRecognizer*)recognizer
{
    GEDCommandResizeSymbol* resizeCommand = [[GEDCommandResizeSymbol alloc]
            initWithSymbol:SERRY.context.symbolHit
           startDimensions:self.initialDimensions
             endDimensions:[SERRY.context.symbolHit getAsRectangle]];
    [SERRY.commandManager addCommand:resizeCommand];
    SERRY.context.graphView.currentState = GEDSelectionState.new;
}

@end
