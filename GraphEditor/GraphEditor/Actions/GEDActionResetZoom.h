//
// Created by cicakmarko on 10/23/13.
//
//  Copyright (c) 2013 CenterDevice GmbH. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GEDActionAbstract.h"


@interface GEDActionResetZoom : GEDActionAbstract
@end
