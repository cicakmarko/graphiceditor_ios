//
// Created by cicakmarko on 6/14/13.
//
//  Copyright (c) 2013 CenterDevice GmbH. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GEDActionAbstract.h"


@interface GEDActionDeleteSelection : GEDActionAbstract
@end
