//
// Created by cicakmarko on 10/23/13.
//
//  Copyright (c) 2013 CenterDevice GmbH. All rights reserved.
//

#import "GEDActionMenuEdit.h"
#import "GEDEditViewController.h"

@interface GEDActionMenuEdit ()
@property(nonatomic, weak) id anchor;
@end

@implementation GEDActionMenuEdit

- (id) initWithAnchor:(id)anchor
{
    if (self = [super init])
    {
        _anchor = anchor;
    }
    return self;
}

- (void) performIfNoPopoverVisible
{
    UIStoryboard* storyboard = [UIStoryboard storyboardWithName:GEDStoryboardMain bundle:nil];
    GEDEditViewController* vc = [storyboard instantiateViewControllerWithIdentifier:@"EditViewControllerSID"];
    [self presentInPopover:vc fromAnchor:self.anchor];
}

@end
