//
//  GEDSelectionHandleHandler.m
//  GraphEditor
//
//  Created by Marko Cicak on 11/1/12.
//  Copyright (c) 2012 Marko Cicak. All rights reserved.
//

#import <CoreGraphics/CoreGraphics.h>
#import "GEDSelectionHandleHandler.h"
#import "GEDContext.h"
#import "GEDModel.h"
#import "GEDSelectionModel.h"

@interface GEDSelectionHandleHandler ()

+ (void) paintSelectionHandle:(CGContextRef)context position:(CGPoint)position scale:(CGFloat)scale;

+ (CGPoint) getHandlePoint:(CGPoint)topLeft size:(CGSize)size handlePosition:(SymbolHandle)handlePosition;

+ (BOOL) isPointInHandle:(GEDSymbol*)symbol point:(CGPoint)point handle:(SymbolHandle)handle scale:(CGFloat)scale;

@end

@implementation GEDSelectionHandleHandler

+ (void) paintSelectionHandles:(CGContextRef)context
                 withSelection:(GEDSelectionModel*)selection
                      andScale:(CGFloat)scale
{
    for (GEDSymbol* s in selection.elements)
    {
        for (int i = SymbolHandleNorth; i <= SymbolHandleNorthWest; i++)
        {
            [GEDSelectionHandleHandler paintSelectionHandle:context
                                                   position:[GEDSelectionHandleHandler getHandlePoint:s.position
                                                                                                 size:s.size
                                                                                       handlePosition:i]
                                                      scale:scale];
        }
    }
}

+ (SymbolHandle) getHandleForSymbol:(GEDSymbol*)symbol point:(CGPoint)point scale:(CGFloat)scale
{
    for (int i = SymbolHandleNorth; i <= SymbolHandleNorthWest; i++)
    {
        if ([GEDSelectionHandleHandler isPointInHandle:symbol point:point handle:i scale:scale])
        {
            return (SymbolHandle) i;
        }
    }

    return SymbolHandleNone;
}

#pragma mark - Private methods

+ (void) paintSelectionHandle:(CGContextRef)context position:(CGPoint)position scale:(CGFloat)scale
{
    CGFloat size = HANDLE_RENDER_SIZE;
    CGContextSetRGBFillColor(context, 0.0f, 0.0f, 0.0f, 1.0f);
    position = CGPointApplyAffineTransform(position, SERRY.context.transform);
    CGContextFillRect(context, CGRectMake(position.x - size / 2, position.y - size / 2, size, size));
}

+ (CGPoint) getHandlePoint:(CGPoint)topLeft size:(CGSize)size handlePosition:(SymbolHandle)handlePosition
{
    CGFloat x = 0, y = 0;

    // Determine y coordinate

    // If top handles
    if (handlePosition == SymbolHandleNorthWest ||
            handlePosition == SymbolHandleNorth ||
            handlePosition == SymbolHandleNorthEast)
    {
        y = topLeft.y;
    }

    // If central on y axis
    if (handlePosition == SymbolHandleEast || handlePosition == SymbolHandleWest)
    {
        y = topLeft.y + size.height / 2;
    }

    // if bottom
    if (handlePosition == SymbolHandleSouthWest ||
            handlePosition == SymbolHandleSouth ||
            handlePosition == SymbolHandleSouthEast)
    {
        y = topLeft.y + size.height;
    }

    // determine x coordinate

    // if left
    if (handlePosition == SymbolHandleNorthWest ||
            handlePosition == SymbolHandleWest ||
            handlePosition == SymbolHandleSouthWest)
    {
        x = topLeft.x;
    }

    // if central on x axis
    if (handlePosition == SymbolHandleNorth || handlePosition == SymbolHandleSouth)
    {
        x = topLeft.x + size.width / 2;
    }

    // if right
    if (handlePosition == SymbolHandleNorthEast ||
            handlePosition == SymbolHandleEast ||
            handlePosition == SymbolHandleSouthEast)
    {
        x = topLeft.x + size.width;
    }

    return CGPointMake(x, y);
}

+ (BOOL) isPointInHandle:(GEDSymbol*)symbol point:(CGPoint)point handle:(SymbolHandle)handle scale:(CGFloat)scale
{
    CGPoint handleCenter = [GEDSelectionHandleHandler getHandlePoint:symbol.position
                                                                size:symbol.size
                                                      handlePosition:handle];

    return fabsf(point.x - handleCenter.x) <= (HANDLE_TOUCH_SIZE / 2) / scale &&
            fabsf(point.y - handleCenter.y) <= (HANDLE_TOUCH_SIZE / 2) / scale;
}

@end
/**/
