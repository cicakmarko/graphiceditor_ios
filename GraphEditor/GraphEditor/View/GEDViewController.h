//
//  GraphViewController.h
//  GraphEditor
//
//  Created by Marko Cicak on 10/30/12.
//  Copyright (c) 2012 Marko Cicak. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GEDViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIBarButtonItem *btnDelete;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *btnDeleteAll;
@property (weak, nonatomic) IBOutlet UILabel *lblCount;

@end
