//
//  GEDSymbol.h
//  GraphEditor
//
//  Created by Marko Cicak on 10/30/12.
//  Copyright (c) 2012 Marko Cicak. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GEDSymbol : NSObject <NSCopying>

@property(nonatomic, assign) CGPoint position;
@property(nonatomic, assign) CGSize size;
@property(nonatomic, strong) UIColor* color;
@property(nonatomic, copy) NSString* text;

- (GEDSymbol*) initWithPosition:(CGPoint)position size:(CGSize)size color:(UIColor*)color text:(NSString*)text;

- (CGRect) getAsRectangle;

@end
