//
// Created by cicakmarko on 6/14/13.
//
//  Copyright (c) 2013 CenterDevice GmbH. All rights reserved.
//

// Notifications
NSString* const GEDSymbolsAddedNotification = @"GEDSymbolsAddedNotification";
NSString* const GEDSymbolsRemovedNotification = @"GEDSymbolsRemovedNotification";
NSString* const GEDSelectionChangedNotification = @"GEDSelectionChangedNotification";
NSString* const GEDModelUpdatedNotification = @"GEDModelUpdatedNotification";

// Exceptions
NSString* const IllegalArgumentException = @"IllegalArgumentException";
NSString* const IllegalStateException = @"IllegalStateException";

// Storyboards
NSString* const GEDStoryboardMain = @"GEDStoryboard";
