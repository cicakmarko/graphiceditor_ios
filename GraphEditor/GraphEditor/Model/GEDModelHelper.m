//
//  GEDModelHelper.m
//  GraphEditor
//
//  Created by Marko Cicak on 10/31/12.
//  Copyright (c) 2012 Marko Cicak. All rights reserved.
//

#import "GEDModelHelper.h"
#import "GEDView.h"
#import "GEDSymbolPainter.h"

@implementation GEDModelHelper

+ (GEDSymbol*) symbolAtPoint:(CGPoint)point
{
    for (int i = SERRY.context.graphView.painters.count - 1; i >= 0; i--)
    {
        GEDSymbol* s = ((GEDSymbolPainter*) SERRY.context.graphView.painters[i]).symbol;

        if (CGRectContainsPoint([s getAsRectangle], point))
        {
            return s;
        }
    }

    return nil;
}

@end
