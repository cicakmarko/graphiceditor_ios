//
//  GEDView.h
//  GraphEditor
//
//  Created by Marko Cicak on 10/30/12.
//  Copyright (c) 2012 Marko Cicak. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GEDState.h"

@interface GEDTapBeganGesture : UITapGestureRecognizer
@property(nonatomic, assign) CGPoint location;
@end

@interface GEDView : UIView

@property(nonatomic, strong) GEDState* currentState;

- (void) redraw;

/*!
 Used to determine top element at given point.
 @see GEDModelHelper
 @return list of element painters
 */
- (NSArray*) painters;

@end
