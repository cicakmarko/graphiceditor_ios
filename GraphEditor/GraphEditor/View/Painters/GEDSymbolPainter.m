//
// Created by cicakmarko on 10/26/13.
//
//  Copyright (c) 2013 CenterDevice GmbH. All rights reserved.
//

#import "GEDSymbolPainter.h"


@implementation GEDSymbolPainter

- (instancetype) initWithSymbol:(GEDSymbol*)symbol
{
    if (self = [super init])
    {
        _symbol = symbol;
    }
    return self;
}


- (void) draw:(CGContextRef)context
{
    GED_THROW_ABSTRACT_METHOD_EXCEPTION;
}

@end
