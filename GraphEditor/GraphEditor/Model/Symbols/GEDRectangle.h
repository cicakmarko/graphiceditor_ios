//
// Created by cicakmarko on 10/26/13.
//
//  Copyright (c) 2013 CenterDevice GmbH. All rights reserved.
//


@interface GEDRectangle : GEDSymbol
- (GEDRectangle*) initWithPosition:(CGPoint)position size:(CGSize)size color:(UIColor*)color text:(NSString*)text;
@end
