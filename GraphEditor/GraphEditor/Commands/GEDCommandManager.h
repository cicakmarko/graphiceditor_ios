//
// Created by cicakmarko on 10/28/13.
//
//  Copyright (c) 2013 CenterDevice GmbH. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol GEDCommand;


@interface GEDCommandManager : NSObject

- (void) addCommand:(id <GEDCommand>)command;

- (void) doCommand;

- (void) undoCommand;

- (BOOL) currentCommandIsFirst;

- (BOOL) currentCommandIsLast;

- (BOOL) isEmpty;

- (NSUInteger) count;

@end
