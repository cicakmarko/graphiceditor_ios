//
//  GraphViewController.m
//  GraphEditor
//
//  Created by Marko Cicak on 10/30/12.
//  Copyright (c) 2012 Marko Cicak. All rights reserved.
//

#import "GEDViewController.h"
#import "GEDActionAbstract.h"
#import "GEDActionManager.h"
#import "GEDActionMenuEdit.h"
#import "GEDActionDeleteSelection.h"
#import "GEDActionDeleteAll.h"
#import "GEDView.h"
#import "GEDActionResetZoom.h"
#import "GEDActionMenuFit.h"
#import "GEDSelectionModel.h"
#import "GEDRectangle.h"
#import "GEDActionEditUndo.h"
#import "GEDActionEditRedo.h"
#import "GEDCommandManager.h"
#import "GEDCommandAddSymbols.h"

@interface GEDViewController ()
@property(weak, nonatomic) IBOutlet UIBarButtonItem* btnUndo;
@property(weak, nonatomic) IBOutlet UIBarButtonItem* btnEdit;
@property(weak, nonatomic) IBOutlet UIBarButtonItem* btnRedo;
@property(weak, nonatomic) IBOutlet UIBarButtonItem* btnFit;
@property(weak, nonatomic) IBOutlet UIToolbar* toolbar;
@property(weak, nonatomic) IBOutlet GEDView* gedView;
@end

@implementation GEDViewController

#pragma mark - Life cycle

- (void) viewDidLoad
{
    [super viewDidLoad];
    // workaround to hide gray line between toolbar and status bar in iOS 7
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
    {
        [self hideGrayLineBetweenToolbarAndStatusBar];
    }
}

- (void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    GEDNC_ADD_OBSERVER_SELF(@selector(refreshViews), GEDSymbolsAddedNotification, nil);
    GEDNC_ADD_OBSERVER_SELF(@selector(refreshViews), GEDSymbolsRemovedNotification, nil);
    GEDNC_ADD_OBSERVER_SELF(@selector(refreshViews), GEDSelectionChangedNotification, nil);
    GEDNC_ADD_OBSERVER_SELF(@selector(refreshViews), GEDModelUpdatedNotification, nil);
    [self refreshViews];

    [self addNewSymbol:CGPointMake(40, 40) color:([UIColor redColor])];
    [self addNewSymbol:CGPointMake(774, 554) color:[UIColor blueColor]];
}

- (void) viewWillDisappear:(BOOL)animated
{
    GEDNC_REMOVE_OBSERVER_SELF;
}

- (void) didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL) shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation
{
    return YES;
}

- (void) willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation
                                 duration:(NSTimeInterval)duration
{
    [super willRotateToInterfaceOrientation:toInterfaceOrientation duration:duration];
    [SERRY.context.graphView redraw];
}

#pragma mark - Actions

- (IBAction) undoAction:(id)sender
{
    [[SERRY.actionManager undoAction] perform];
}

- (IBAction) redoAction:(id)sender
{
    [[SERRY.actionManager redoAction] perform];
}

- (IBAction) resetZoomAction:(id)sender
{
    [[SERRY.actionManager resetZoomAction] perform];
}

- (IBAction) fitAction:(id)sender
{
    [[SERRY.actionManager fitActionWithAnchor:self.btnFit] perform];
}

- (IBAction) editAction:(id)sender
{
    [[SERRY.actionManager menuEditActionWithAnchor:self.btnEdit] perform];
}

- (IBAction) deleteAction:(id)sender
{
    [[SERRY.actionManager deleteSelectionAction] perform];
}

- (IBAction) removeAllAction:(id)sender
{
    [[SERRY.actionManager deleteAllAction] perform];
}

#pragma mark - Notifications

- (void) didUpdateModel:(NSNotification*)notification
{
    [self refreshViews];
}

#pragma mark Private methods

- (void) refreshCountLabel
{
    self.lblCount.text = [NSString stringWithFormat:@"%d (%d)",
                                                    SERRY.model.symbols.count,
                                                    SERRY.selection.count];
}

- (void) hideGrayLineBetweenToolbarAndStatusBar
{
    CALayer* layer = self.toolbar.layer.sublayers[1];
    layer.hidden = YES;
    layer.opacity = 0;
}

- (void) refreshViews
{
    [self refreshCountLabel];
    self.btnDelete.enabled = ![SERRY.selection isEmpty];
    self.btnDeleteAll.enabled = SERRY.model.symbols.count != 0;

    self.btnUndo.enabled = ![SERRY.commandManager currentCommandIsFirst];
    self.btnRedo.enabled = ![SERRY.commandManager currentCommandIsLast];
}

- (void) addNewSymbol:(CGPoint)position color:(UIColor*)color
{
    CGSize size = CGSizeMake(200, 100);
    GEDRectangle* rectangle = [[GEDRectangle alloc]
            initWithPosition:position
                        size:size
                       color:color
                        text:[NSString stringWithFormat:@"Symbol %d", SERRY.context.symbolIndex++]];
    [SERRY.commandManager addCommand:[[GEDCommandAddSymbols alloc] initWithSymbols:@[ rectangle ]]];
}

@end
