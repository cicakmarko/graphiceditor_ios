//
// Created by Marko Cicak on 11/1/15.
// Copyright (c) 2015 Marko Cicak. All rights reserved.
//

#import "GEDLasoSelectionState.h"
#import "GEDSelectionModel.h"
#import "GEDView.h"
#import "GEDSelectionState.h"

@interface GEDLasoSelectionState ()
@property(nonatomic, assign) CGPoint startingPoint;
@end

@implementation GEDLasoSelectionState

- (void) stateStarted
{
    SERRY.context.laso = CGRectMake(-10, -10, 0, 0); // reset old laso
    [SERRY.selection clearSelection];
    SERRY.context.lasoOn = YES;
    self.startingPoint = CGPointApplyAffineTransform(SERRY.context.lastPosition, SERRY.context.transform);
}

- (void) stateFinished
{
    NSMutableArray* toSelect = [NSMutableArray array];
    CGRect lasoUserSpace = CGRectApplyAffineTransform(SERRY.context.laso,
                                                      CGAffineTransformInvert(SERRY.context.transform));
    for (GEDSymbol* s in SERRY.model.symbols)
    {
        if (CGRectIntersectsRect(lasoUserSpace, [s getAsRectangle]))
        {
            [toSelect addObject:s];
        }
    }
    [SERRY.selection addMultipleSelection:toSelect];

    SERRY.context.lasoOn = NO;
}

- (void) panBegan:(UIPanGestureRecognizer*)recognizer
{
    [super panBegan:recognizer];
}

- (void) panChanged:(UIPanGestureRecognizer*)recognizer
{
    CGPoint newPosition = [recognizer locationInView:SERRY.context.graphView];
    // update positions
    SERRY.context.laso = CGRectMake(self.startingPoint.x, self.startingPoint.y,
                                    newPosition.x - self.startingPoint.x, newPosition.y - self.startingPoint.y);

    [SERRY.context.graphView setNeedsDisplay];
}

- (void) panEnded:(UIPanGestureRecognizer*)recognizer
{
    SERRY.context.graphView.currentState = GEDSelectionState.new;
}

@end
