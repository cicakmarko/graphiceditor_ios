//
// Created by cicakmarko on 6/14/13.
//
//  Copyright (c) 2013 CenterDevice GmbH. All rights reserved.
//

#import "GEDActionDeleteAll.h"
#import "GEDCommandDeleteSymbols.h"
#import "GEDCommandManager.h"


@implementation GEDActionDeleteAll

- (void) perform
{
    GEDCommandDeleteSymbols* deleteCommand = [[GEDCommandDeleteSymbols alloc]
            initWithSymbols:SERRY.model.symbols];
    [SERRY.commandManager addCommand:deleteCommand];
}

@end
