//
// Created by cicakmarko on 10/25/13.
//
//  Copyright (c) 2013 CenterDevice GmbH. All rights reserved.
//

#import "GEDActionEditCut.h"
#import "GEDClipboard.h"
#import "GEDSelectionModel.h"
#import "GEDCommandDeleteSymbols.h"
#import "GEDCommandManager.h"


@implementation GEDActionEditCut

- (void) perform
{
    [SERRY.clipboard setContent:SERRY.selection.elements];

    GEDCommandDeleteSymbols* deleteCommand = [[GEDCommandDeleteSymbols alloc] initWithSymbols:SERRY.selection.elements];
    [SERRY.commandManager addCommand:deleteCommand];
}

@end
