//
// Created by cicakmarko on 10/23/13.
//
//  Copyright (c) 2013 CenterDevice GmbH. All rights reserved.
//

#import <Foundation/Foundation.h>

@class GEDModel;
@class GEDContext_Old;
@class GEDActionManager;
@class GEDClipboard;
@class GEDSelectionModel;
@class GEDCommandManager;
@class GEDContext;


@interface GEDServiceRegistry : NSObject

@property(nonatomic, strong) GEDModel* model;
@property(nonatomic, strong) GEDSelectionModel* selection;
@property(nonatomic, strong) GEDActionManager* actionManager;
@property(nonatomic, strong) GEDCommandManager* commandManager;
@property(nonatomic, strong) GEDClipboard* clipboard;
@property(nonatomic, strong) GEDContext* context;

+ (GEDServiceRegistry*) instance;

@end
