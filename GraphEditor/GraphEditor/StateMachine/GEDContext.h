//
// Created by Marko Cicak on 11/1/15.
// Copyright (c) 2015 Marko Cicak. All rights reserved.
//

#import <Foundation/Foundation.h>

@class GEDSymbol;
@class GEDView;


@interface GEDContext : NSObject

@property(nonatomic, assign) CGPoint lastPosition;
@property(nonatomic, strong) GEDView* graphView;
@property(nonatomic, strong) GEDSymbol* symbolHit;

// laso params
@property(nonatomic, assign) BOOL lasoOn;
@property(nonatomic, assign) CGRect laso;

// zoom parameters
@property(nonatomic, assign) CGFloat scale;
@property(nonatomic, assign) CGAffineTransform transform;

/** Currently presented popover controller and action sheet. */
@property(nonatomic, strong) UIPopoverController* popoverController;
@property(nonatomic, strong) UIActionSheet* actionSheet;

@property(nonatomic, assign) NSInteger symbolIndex;

@end
