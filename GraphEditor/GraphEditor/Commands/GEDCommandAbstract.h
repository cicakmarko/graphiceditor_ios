//
// Created by cicakmarko on 10/28/13.
//
//  Copyright (c) 2013 CenterDevice GmbH. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GEDCommand.h"


@interface GEDCommandAbstract : NSObject <GEDCommand>
@end
