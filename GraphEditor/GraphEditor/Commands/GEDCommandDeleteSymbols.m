//
// Created by cicakmarko on 10/29/13.
//
//  Copyright (c) 2013 CenterDevice GmbH. All rights reserved.
//

#import "GEDCommandDeleteSymbols.h"
#import "GEDSelectionModel.h"

@interface GEDCommandDeleteSymbols ()
@property(nonatomic, strong) NSArray* symbols;
@end


@implementation GEDCommandDeleteSymbols

- (instancetype) initWithSymbols:(NSArray*)symbols
{
    if (self = [super init])
    {
        _symbols = [symbols copy];
    }
    return self;
}

- (void) doCommand
{
    [SERRY.selection clearSelection];
    [SERRY.model removeSymbols:self.symbols];
}

- (void) undoCommand
{
    [SERRY.selection clearSelection];
    [SERRY.model addSymbols:self.symbols];
}

@end
