//
// Created by cicakmarko on 10/25/13.
//
//  Copyright (c) 2013 CenterDevice GmbH. All rights reserved.
//

#import "GEDClipboard.h"

@interface GEDClipboard ()
@property(nonatomic, strong) NSMutableArray* elements;
@end

@implementation GEDClipboard

- (void) setContent:(NSArray*)elements
{
    _elements = [[NSMutableArray alloc] initWithArray:elements copyItems:YES];
}

- (NSArray*) content
{
    return [[NSArray alloc] initWithArray:self.elements copyItems:YES];
}

- (void) clear
{
    [self.elements removeAllObjects];
}

@end
