//
// Created by cicakmarko on 10/29/13.
//
//  Copyright (c) 2013 CenterDevice GmbH. All rights reserved.
//

#import "GEDCommandResizeSymbol.h"

@interface GEDCommandResizeSymbol ()
@property(nonatomic, strong) GEDSymbol* symbol;
@property(nonatomic) CGRect startDimensions;
@property(nonatomic) CGRect endDimensions;
@property(nonatomic, assign) BOOL firstRun;
@end


@implementation GEDCommandResizeSymbol

- (instancetype) initWithSymbol:(GEDSymbol*)symbol
                startDimensions:(CGRect)startDimensions
                  endDimensions:(CGRect)endDimensions
{
    if (self = [super init])
    {
        _symbol = symbol;
        _startDimensions = startDimensions;
        _endDimensions = endDimensions;
        _firstRun = YES;
    }
    return self;
}

- (void) doCommand
{
    if (self.firstRun)
    {
        self.firstRun = NO;
        return;
    }

    self.symbol.position = self.endDimensions.origin;
    self.symbol.size = self.endDimensions.size;
    [SERRY.model fireModelUpdate];
}

- (void) undoCommand
{
    self.symbol.position = self.startDimensions.origin;
    self.symbol.size = self.startDimensions.size;
    [SERRY.model fireModelUpdate];
}

@end
