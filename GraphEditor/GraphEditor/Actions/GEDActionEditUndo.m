//
// Created by cicakmarko on 10/28/13.
//
//  Copyright (c) 2013 CenterDevice GmbH. All rights reserved.
//

#import "GEDActionEditUndo.h"
#import "GEDCommandManager.h"


@implementation GEDActionEditUndo

- (void) perform
{
    [SERRY.commandManager undoCommand];
}

@end
