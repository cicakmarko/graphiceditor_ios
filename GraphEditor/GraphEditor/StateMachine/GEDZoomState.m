//
// Created by Marko Cicak on 11/1/15.
// Copyright (c) 2015 Marko Cicak. All rights reserved.
//

#import "GEDZoomState.h"
#import "GEDSelectionState.h"
#import "GEDView.h"

#define SCALING_FACTOR  1.07;

@interface GEDZoomState ()
@property(nonatomic, assign) CGFloat initialScale;
@end

@implementation GEDZoomState

- (void) stateStarted
{
    self.initialScale = SERRY.context.scale;
}

- (void) pinchChanged:(UIPinchGestureRecognizer*)recognizer
{
    CGPoint middlePoint = [recognizer locationInView:SERRY.context.graphView];

    CGFloat scale = self.initialScale + recognizer.scale - 1;
    if (scale < 0.1)
    {
        return;
    }
    SERRY.context.scale = scale;

    CGPoint oldPosition = [self transformToUserSpace:middlePoint];
    SERRY.context.transform = CGAffineTransformMakeScale(scale, scale);
    CGPoint newPosition = [self transformToUserSpace:middlePoint];
    SERRY.context.transform = CGAffineTransformTranslate(SERRY.context.transform,
                                                         newPosition.x - oldPosition.x,
                                                         newPosition.y - oldPosition.y);

    [SERRY.model fireModelUpdate];
}

- (void) pinchEnded:(UIPinchGestureRecognizer*)recognizer
{
    SERRY.context.graphView.currentState = GEDSelectionState.new;
}

@end
