//
// Created by cicakmarko on 10/28/13.
//
//  Copyright (c) 2013 CenterDevice GmbH. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol GEDCommand <NSObject>

- (void) doCommand;

- (void) undoCommand;

@end
