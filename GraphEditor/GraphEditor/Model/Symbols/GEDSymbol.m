//
//  GEDSymbol.m
//  GraphEditor
//
//  Created by Marko Cicak on 10/30/12.
//  Copyright (c) 2012 Marko Cicak. All rights reserved.
//

@implementation GEDSymbol


- (GEDSymbol*) initWithPosition:(CGPoint)position size:(CGSize)size color:(UIColor*)color text:(NSString*)text
{
    if (self = [super init])
    {
        _position = position;
        _size = size;
        _color = color;
        _text = text;
    }
    return self;
}

- (CGRect) getAsRectangle
{
    GED_THROW_ABSTRACT_METHOD_EXCEPTION;
}

/*- (NSString*) description
{
    return [NSString stringWithFormat:@"[%f, %f, %f, %f", self.position.x, self.position.y, self.size.width, self.size.height];
}*/

#pragma mark - NSCopying

- (id) copyWithZone:(NSZone*)zone
{
    GEDSymbol* copy = [[[self class] alloc] init];
    if (copy)
    {
        copy.position = self.position;
        copy.size = self.size;
        copy.color = [self.color copy];
        copy.text = self.text;
    }
    return copy;
}

@end
