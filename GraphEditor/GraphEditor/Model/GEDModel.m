//
//  GEDModel.m
//  GraphEditor
//
//  Created by Marko Cicak on 10/30/12.
//  Copyright (c) 2012 Marko Cicak. All rights reserved.
//

#import "GEDSelectionModel.h"

@implementation GEDModel

- (id) init
{
    if (self = [super init])
    {
        _symbols = [NSMutableArray array];
    }
    return self;
}

#pragma mark - GEDModel manipulation

- (void) addSymbol:(GEDSymbol*)symbol
{
    [self.symbols addObject:symbol];
    [self fireSymbolsAdded:@[ symbol ]];
}

- (void) addSymbols:(NSArray*)symbols
{
    [self.symbols addObjectsFromArray:symbols];
    [self fireSymbolsAdded:symbols];
}

- (void) removeSymbol:(GEDSymbol*)symbol
{
    [self.symbols removeObject:symbol];
    [self fireSymbolsRemoved:@[ symbol ]];
}

- (void) removeSymbols:(NSArray*)symbols
{
    [self.symbols removeObjectsInArray:symbols];
    [self fireSymbolsRemoved:symbols];
}

- (void) removeAllSymbols
{
    NSArray* removed = self.symbols;
    [self.symbols removeAllObjects];
    [self fireSymbolsRemoved:removed];
}

- (void) removeSelected
{
    NSArray* removed = [SERRY.selection elements];
    [self.symbols removeObjectsInArray:[SERRY.selection elements]];
    [SERRY.selection clearSelection];
    [self fireSymbolsRemoved:removed];
}

#pragma mark - Update methods

- (void) fireSymbolsAdded:(NSArray*)symbols
{
    GEDNC_POST_NOTIFICATION(GEDSymbolsAddedNotification, nil, @{ @"symbols" : symbols });
}

- (void) fireSymbolsRemoved:(NSArray*)symbols
{
    GEDNC_POST_NOTIFICATION(GEDSymbolsRemovedNotification, nil, @{ @"symbols" : symbols });
}

- (void) fireModelUpdate
{
    GEDNC_POST_NOTIFICATION(GEDModelUpdatedNotification, nil, nil);
}

@end
