//
//  AppDelegate.h
//  GraphEditor
//
//  Created by Marko Cicak on 10/30/12.
//  Copyright (c) 2012 Marko Cicak. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property(strong, nonatomic) UIWindow* window;

@end
