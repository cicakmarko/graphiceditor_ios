//
//  GEDModel.h
//  GraphEditor
//
//  Created by Marko Cicak on 10/30/12.
//  Copyright (c) 2012 Marko Cicak. All rights reserved.
//

#import "GEDSymbol.h"

@interface GEDModel : NSObject

@property(nonatomic, strong) NSMutableArray* symbols;

- (void) addSymbol:(GEDSymbol*)symbol;

- (void) addSymbols:(NSArray*)symbols;

- (void) removeSymbol:(GEDSymbol*)symbol;

- (void) removeSymbols:(NSArray*)symbols;

- (void) removeAllSymbols;

- (void) removeSelected;

- (void) fireModelUpdate;

@end
