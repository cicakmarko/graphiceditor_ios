//
// Created by cicakmarko on 10/29/13.
//
//  Copyright (c) 2013 CenterDevice GmbH. All rights reserved.
//

#import "GEDCommandMoveSymbols.h"

@interface GEDCommandMoveSymbols ()
@property(nonatomic, strong) NSArray* symbols;
@property(nonatomic) CGFloat dx;
@property(nonatomic) CGFloat dy;
@property(nonatomic, assign) BOOL firstRun;
@end


@implementation GEDCommandMoveSymbols

- (instancetype) initWithSymbols:(NSArray*)symbols deltaX:(CGFloat)x deltaY:(CGFloat)y
{
    if (self = [super init])
    {
        _symbols = [symbols copy];
        _dx = x;
        _dy = y;
        _firstRun = YES;
    }
    return self;
}

- (void) doCommand
{
    if (self.firstRun)
    {
        self.firstRun = NO;
        return;
    }

    for (GEDSymbol* symbol in self.symbols)
    {
        symbol.position = CGPointMake(symbol.position.x + self.dx, symbol.position.y + self.dy);
    }
    [SERRY.model fireModelUpdate];
}

- (void) undoCommand
{
    for (GEDSymbol* symbol in self.symbols)
    {
        symbol.position = CGPointMake(symbol.position.x - self.dx, symbol.position.y - self.dy);
    }
    [SERRY.model fireModelUpdate];
}

@end
