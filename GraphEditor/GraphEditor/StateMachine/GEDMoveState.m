//
// Created by Marko Cicak on 11/1/15.
// Copyright (c) 2015 Marko Cicak. All rights reserved.
//

#import "GEDMoveState.h"
#import "GEDSelectionModel.h"
#import "GEDSelectionState.h"
#import "GEDView.h"
#import "GEDCommandManager.h"
#import "GEDCommandMoveSymbols.h"


@interface GEDMoveState ()
@property(nonatomic, assign) CGPoint oldPosition;
@property(nonatomic, assign) CGPoint newPosition;
@property(nonatomic, assign) CGFloat total_x;
@property(nonatomic, assign) CGFloat total_y;
@end

@implementation GEDMoveState

- (void) stateStarted
{
    self.newPosition = SERRY.context.lastPosition;
    self.total_x = 0.0;
    self.total_y = 0.0;
}

- (void) panChanged:(UIPanGestureRecognizer*)recognizer
{
    self.oldPosition = self.newPosition;
    CGPoint point = [recognizer locationInView:(id) SERRY.context.graphView];
    self.newPosition = [self transformToUserSpace:point];

    CGFloat dx = self.newPosition.x - self.oldPosition.x;
    CGFloat dy = self.newPosition.y - self.oldPosition.y;

    self.total_x += dx;
    self.total_y += dy;

    // update positions
    for (GEDSymbol* s in [SERRY.selection elements])
    {
        s.position = CGPointMake(s.position.x + dx, s.position.y + dy);
    }

    [SERRY.model fireModelUpdate];
}

- (void) panEnded:(UIPanGestureRecognizer*)recognizer
{
    GEDCommandMoveSymbols* moveCommand = [[GEDCommandMoveSymbols alloc]
            initWithSymbols:SERRY.selection.elements deltaX:self.total_x deltaY:self.total_y];
    [SERRY.commandManager addCommand:moveCommand];
    SERRY.context.graphView.currentState = GEDSelectionState.new;
}

@end
