//
// Created by cicakmarko on 10/26/13.
//
//  Copyright (c) 2013 CenterDevice GmbH. All rights reserved.
//

#import "GEDRectangle.h"


@implementation GEDRectangle

- (GEDRectangle*) initWithPosition:(CGPoint)position size:(CGSize)size color:(UIColor*)color text:(NSString*)text
{
    self = [super initWithPosition:position size:size color:color text:text];
    if (self)
    {

    }

    return self;
}

- (CGRect) getAsRectangle
{
    return CGRectMake(self.position.x, self.position.y, self.size.width, self.size.height);
}

@end
