//
// Created by cicakmarko on 10/24/13.
//
//  Copyright (c) 2013 CenterDevice GmbH. All rights reserved.
//

#import "GEDActionMenuFit.h"
#import "GEDFitViewController.h"

@interface GEDActionMenuFit ()
@property(nonatomic, strong) UIBarButtonItem* anchor;
@end

@implementation GEDActionMenuFit

- (instancetype) initWithAnchor:(UIBarButtonItem*)anchor
{
    if (self = [super init])
    {
        _anchor = anchor;
    }
    return self;
}

- (void) performIfNoPopoverVisible
{
    UIStoryboard* storyboard = [UIStoryboard storyboardWithName:GEDStoryboardMain bundle:nil];
    GEDFitViewController* vc = [storyboard instantiateViewControllerWithIdentifier:@"FitViewControllerSID"];
    [self presentInPopover:vc fromAnchor:self.anchor];
}

@end
