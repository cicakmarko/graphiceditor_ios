//
// Created by Marko Cicak on 11/1/15.
// Copyright (c) 2015 Marko Cicak. All rights reserved.
//

#import "GEDSelectionState.h"
#import "GEDModelHelper.h"
#import "GEDSelectionModel.h"
#import "GEDSelectionHandleHandler.h"
#import "GEDCommandAddSymbols.h"
#import "GEDCommandManager.h"
#import "GEDRectangle.h"
#import "GEDView.h"
#import "GEDLasoSelectionState.h"
#import "GEDMoveState.h"
#import "GEDZoomState.h"
#import "GEDResizeState.h"

@interface GEDSelectionState ()
@property(nonatomic, assign) BOOL freshlySelected;
@property(nonatomic, assign) BOOL freeToAdd;
@end

@implementation GEDSelectionState

- (void) stateStarted
{
    self.freshlySelected = NO;
    self.freeToAdd = YES;
}

- (void) tapBegan:(UITapGestureRecognizer*)recognizer
{
    CGPoint point = [recognizer locationInView:(id) SERRY.context.graphView];
    SERRY.context.lastPosition = [self transformToUserSpace:point];
    SERRY.context.symbolHit = [GEDModelHelper symbolAtPoint:SERRY.context.lastPosition];

    if (SERRY.selection.count > 0)
    {
        for (GEDSymbol* s in SERRY.selection.elements)
        {
            SymbolHandle handle = [GEDSelectionHandleHandler getHandleForSymbol:s
                                                                          point:SERRY.context.lastPosition
                                                                          scale:1];
            if (handle != SymbolHandleNone)
            {
                // perhaps there will be resizing so don't clear the selection
                return;
            }
        }
        if (!SERRY.context.symbolHit)
        {
            self.freeToAdd = NO;
            [SERRY.selection clearSelection];
            return;
        }
    }

    if (SERRY.context.symbolHit != nil)
    {
        if (![SERRY.selection.elements containsObject:SERRY.context.symbolHit])
        {
            self.freeToAdd = NO;
            [SERRY.selection addToSelection:SERRY.context.symbolHit];
            self.freshlySelected = YES;
        }
        return;
    }
}

- (void) tapEnded:(UITapGestureRecognizer*)recognizer
{
    if (!self.freshlySelected && SERRY.context.symbolHit != nil)
    {
        [SERRY.selection removeFromSelection:SERRY.context.symbolHit];
        return;
    }
    self.freshlySelected = NO;

    // add new symbol
    if (self.freeToAdd)
    {
        [self addNewSymbol];
    }
    else
    {
        self.freeToAdd = YES;
    }
}

- (void) pinchBegan:(UIPinchGestureRecognizer*)recognizer
{
    SERRY.context.graphView.currentState = GEDZoomState.new;
}

- (void) panBegan:(UIPanGestureRecognizer*)recognizer
{
    if (SERRY.selection.count > 0)
    {
        for (GEDSymbol* s in SERRY.selection.elements)
        {
            SymbolHandle handle = [GEDSelectionHandleHandler getHandleForSymbol:s
                                                                          point:SERRY.context.lastPosition
                                                                          scale:SERRY.context.scale];
            if (handle != SymbolHandleNone)
            {
                SERRY.context.symbolHit = s;
                GEDResizeState* resizeState = GEDResizeState.new;
                resizeState.handle = handle;
                SERRY.context.graphView.currentState = resizeState;
                return;
            }
        }
    }

    if (SERRY.context.symbolHit == nil)
    {
        SERRY.context.graphView.currentState = GEDLasoSelectionState.new;
    }
    else
    {
        SERRY.context.graphView.currentState = GEDMoveState.new;
    }
}

#pragma mark - Private

- (void) addNewSymbol
{
    CGSize size = CGSizeMake(200, 100);
    CGPoint position = CGPointMake(SERRY.context.lastPosition.x - size.width / 2,
                                   SERRY.context.lastPosition.y - size.height / 2);
    UIColor* color = [UIColor greenColor];
    GEDRectangle* rectangle = [[GEDRectangle alloc]
            initWithPosition:position
                        size:size
                       color:color
                        text:[NSString stringWithFormat:@"Symbol %d", SERRY.context.symbolIndex++]];
    [SERRY.commandManager addCommand:[[GEDCommandAddSymbols alloc] initWithSymbols:@[ rectangle ]]];
}

@end
