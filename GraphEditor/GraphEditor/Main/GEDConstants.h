//
// Created by cicakmarko on 6/14/13.
//
//  Copyright (c) 2013 CenterDevice GmbH. All rights reserved.
//

#ifndef GED_CONSTANTS_H
#define GED_CONSTANTS_H

// Notifications
FOUNDATION_EXPORT NSString* const GEDSymbolsAddedNotification;
FOUNDATION_EXPORT NSString* const GEDSymbolsRemovedNotification;
FOUNDATION_EXPORT NSString* const GEDSelectionChangedNotification;
FOUNDATION_EXPORT NSString* const GEDModelUpdatedNotification;

// Exceptions
FOUNDATION_EXPORT NSString* const IllegalArgumentException;
FOUNDATION_EXPORT NSString* const IllegalStateException;

// Storyboards
FOUNDATION_EXPORT NSString* const GEDStoryboardMain;

#endif
