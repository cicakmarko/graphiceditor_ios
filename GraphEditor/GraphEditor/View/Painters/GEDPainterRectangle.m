//
// Created by cicakmarko on 10/26/13.
//
//  Copyright (c) 2013 CenterDevice GmbH. All rights reserved.
//

#import "GEDPainterRectangle.h"


@implementation GEDPainterRectangle

- (void) draw:(CGContextRef)context
{
    CGContextSaveGState(context);

    const CGFloat* components = CGColorGetComponents([self.symbol.color CGColor]);

    CGContextSetRGBFillColor(context, 0.0f, 0.0f, 0.0f, 1.0f);

    GEDContext* ctx = SERRY.context;

    CGPoint transformedPosition = CGPointApplyAffineTransform(self.symbol.position, ctx.transform);
    CGSize transformedSize = CGSizeApplyAffineTransform(self.symbol.size, ctx.transform);

    CGContextFillRect(context,
                      CGRectMake(transformedPosition.x,
                                 transformedPosition.y,
                                 transformedSize.width,
                                 transformedSize.height));

    CGContextSetRGBFillColor(context, components[0], components[1], components[2], 1.0f);

    CGRect contextRect = CGRectMake(transformedPosition.x + 2,
                                    transformedPosition.y + 2,
                                    transformedSize.width - 4,
                                    transformedSize.height - 4);

    CGContextFillRect(context, contextRect);

    [self drawCenteredTextWithContext:context inRectangle:contextRect];

    CGContextRestoreGState(context);
}

- (void) drawCenteredTextWithContext:(CGContextRef)context inRectangle:(CGRect)contextRect
{
    CGFloat yOffset = contextRect.origin.y + (contextRect.size.height) / 2.0;
    CGRect textRect = CGRectMake(contextRect.origin.x, yOffset, contextRect.size.width, contextRect.size.height);

    CGAffineTransform t = SERRY.context.transform;
    // Need to conjugate D value, otherwise text is upside down
    CGAffineTransform textTransform = CGAffineTransformMake(t.a, t.b, t.c, -t.d, t.tx, t.ty);

    CGContextSelectFont(context, "Helvetica", 16, kCGEncodingMacRoman);
    CGContextSetCharacterSpacing(context, 2);
    CGContextSetTextDrawingMode(context, kCGTextFillStroke);
    CGContextSetRGBFillColor(context, 0, 0, 0, 1);
    CGContextSetRGBStrokeColor(context, 0, 0, 0, 1);
    CGContextSetTextMatrix(context, textTransform);

    // In order to draw text centered in rectangle, we first draw it invisible to get text width so we can
    // calculate on which point to start drawing text in CGContextShowTextAtPoint method
    NSString* text = self.symbol.text;
    CGContextSetTextDrawingMode(context, kCGTextInvisible);
    CGContextShowTextAtPoint(context, 0, 0, [text UTF8String], strlen([text UTF8String]));

    CGPoint pt = CGContextGetTextPosition(context); // text end point
    CGContextSetTextDrawingMode(context, kCGTextFill);

    CGContextShowTextAtPoint(context,
                             (textRect.origin.x + contextRect.size.width / 2) - pt.x / 2,
                             textRect.origin.y + pt.y / 2,
                             [self.symbol.text UTF8String],
                             [self.symbol.text length]);
}

@end
