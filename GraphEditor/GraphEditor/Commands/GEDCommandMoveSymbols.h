//
// Created by cicakmarko on 10/29/13.
//
//  Copyright (c) 2013 CenterDevice GmbH. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GEDCommandAbstract.h"


@interface GEDCommandMoveSymbols : GEDCommandAbstract

- (instancetype) initWithSymbols:(NSArray*)symbols deltaX:(CGFloat)x deltaY:(CGFloat)y;

@end
