//
// Created by cicakmarko on 10/25/13.
//
//  Copyright (c) 2013 CenterDevice GmbH. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface GEDClipboard : NSObject

- (void) setContent:(NSArray*)elements;

- (NSArray*) content;

- (void) clear;

@end
