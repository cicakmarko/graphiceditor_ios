//
// Created by cicakmarko on 10/28/13.
//
//  Copyright (c) 2013 CenterDevice GmbH. All rights reserved.
//

#import "GEDActionEditRedo.h"
#import "GEDCommandManager.h"


@implementation GEDActionEditRedo

- (void) perform
{
    [SERRY.commandManager doCommand];
}

@end
