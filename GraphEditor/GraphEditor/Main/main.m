//
//  main.m
//  GraphEditor
//
//  Created by Marko Cicak on 10/30/12.
//  Copyright (c) 2012 Marko Cicak. All rights reserved.
//

#import "AppDelegate.h"

int main(int argc, char* argv[]) {
    @autoreleasepool
    {
        @try
        {
            return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
        }
        @catch (NSException* exception)
        {
            NSArray* backtrace = [exception callStackSymbols];
            NSString* version = [[UIDevice currentDevice] systemVersion];
            NSString* message = [NSString stringWithFormat:@"OS: %@. Backtrace:\n%@",
                                                           version,
                                                           backtrace];
            NSLog(@"Exception caught in main() - %@", [exception description]);
            NSLog(@"%@", message);
            return 1;
        }
    }
}
