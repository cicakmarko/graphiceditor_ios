//
// Created by cicakmarko on 6/14/13.
//
//  Copyright (c) 2013 CenterDevice GmbH. All rights reserved.
//

#import "GEDActionManager.h"
#import "GEDActionDeleteSelection.h"
#import "GEDActionDeleteAll.h"
#import "GEDActionMenuEdit.h"
#import "GEDActionEditSelectAll.h"
#import "GEDActionResetZoom.h"
#import "GEDActionMenuFit.h"
#import "GEDActionZoomToFit.h"
#import "GEDActionEditPaste.h"
#import "GEDActionEditCopy.h"
#import "GEDActionEditCut.h"
#import "GEDActionEditUndo.h"
#import "GEDActionEditRedo.h"


@implementation GEDActionManager

- (GEDActionDeleteSelection*) deleteSelectionAction
{
    return [[GEDActionDeleteSelection alloc] init];
}

- (GEDActionDeleteAll*) deleteAllAction
{
    return [[GEDActionDeleteAll alloc] init];
}

- (GEDActionMenuEdit*) menuEditActionWithAnchor:(id)anchor
{
    return [[GEDActionMenuEdit alloc] initWithAnchor:anchor];
}

- (GEDActionEditSelectAll*) selectAllAction
{
    return [[GEDActionEditSelectAll alloc] init];
}

- (GEDActionResetZoom*) resetZoomAction
{
    return [[GEDActionResetZoom alloc] init];
}

- (GEDActionMenuFit*) fitActionWithAnchor:(UIBarButtonItem*)anchor
{
    return [[GEDActionMenuFit alloc] initWithAnchor:anchor];
}

- (GEDActionZoomToFit*) fitZoomActionWithElements:(NSArray*)elements
{
    return [[GEDActionZoomToFit alloc] initWithElements:elements];
}

- (GEDActionEditCut*) cutAction
{
    return [[GEDActionEditCut alloc] init];
}

- (GEDActionEditCopy*) copyAction
{
    return [[GEDActionEditCopy alloc] init];
}

- (GEDActionEditPaste*) pasteAction
{
    return [[GEDActionEditPaste alloc] init];
}

- (GEDActionEditUndo*) undoAction
{
    return [[GEDActionEditUndo alloc] init];
}

- (GEDActionEditRedo*) redoAction
{
    return [[GEDActionEditRedo alloc] init];
}

@end
