//
// Created by cicakmarko on 10/24/13.
//
//  Copyright (c) 2013 CenterDevice GmbH. All rights reserved.
//

#import "GEDActionZoomToFit.h"
#import "GEDView.h"

@interface GEDActionZoomToFit ()
@property(nonatomic, strong) NSArray* elements;
@end

@implementation GEDActionZoomToFit

- (instancetype) initWithElements:(NSArray*)elements
{
    if (self = [super init])
    {
        _elements = elements;
    }
    return self;
}

- (void) perform
{
    if (self.elements.count == 0)
    {
        return;
    }

    GEDSymbol* first = self.elements[0];
    CGFloat x1 = first.position.x;
    CGFloat y1 = first.position.y;
    CGFloat x2 = x1 + first.size.width;
    CGFloat y2 = y1 + first.size.height;

    for (GEDSymbol* symbol in self.elements)
    {
        x1 = MIN(symbol.position.x, x1);
        y1 = MIN(symbol.position.y, y1);
        x2 = MAX(symbol.position.x + symbol.size.width, x2);
        y2 = MAX(symbol.position.y + symbol.size.height, y2);
    }

    CGFloat width = x2 - x1;
    CGFloat height = y2 - y1;
    CGFloat viewWidth = SERRY.context.graphView.bounds.size.width;
    CGFloat viewHeight = SERRY.context.graphView.bounds.size.height;

    double scaleX = viewWidth / width;
    double scaleY = viewHeight / height;
    CGFloat scale = (CGFloat) MIN(scaleX, scaleY);

    SERRY.context.transform = CGAffineTransformMakeScale(scale, scale);
    SERRY.context.scale = scale;
    SERRY.context.transform = CGAffineTransformTranslate(SERRY.context.transform, -x1, -y1);

    [SERRY.model fireModelUpdate];
}

@end
