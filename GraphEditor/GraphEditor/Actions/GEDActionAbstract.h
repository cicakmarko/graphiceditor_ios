//
// Created by cicakmarko on 6/14/13.
//
//  Copyright (c) 2013 CenterDevice GmbH. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface GEDActionAbstract : NSObject

/*
 Default implementation checks if any popover or action sheet is visible. If none is visible,
 performIfNoPopoverVisible is called.
 If you don't care if any popover or action sheet is visible, override this method.
 */
- (void) perform;

/*
 Override this method to remove view from views hierarchy.
 */
- (void) performHide;

/*
 Override this method if you want to make sure execution will perform only if no popover or action sheet is visible.
 */
- (void) performIfNoPopoverVisible;

/**
 Helper method to be called by subclasses. Will present the provided view controller in a popover
 from the given anchor.

 @param controller a view controller to be presented in a popover
 @param anchor origin for the popover - can be a UIView or a UIBarButtonItem
 */
- (void) presentInPopover:(UIViewController*)controller fromAnchor:(id)anchor;

/**
 Helper method to be called by subclasses. Will present the provided view controller in a popover
 from the given anchor.

 @param controller a view controller to be presented in a popover
 @param anchor origin for the popover - can be a UIView or a UIBarButtonItem
 @param delegate optional delegate for the popover view controller that is created
 */
- (void) presentInPopover:(UIViewController*)controller
               fromAnchor:(id)anchor
          popoverDelegate:(id <UIPopoverControllerDelegate>)delegate;

@end
